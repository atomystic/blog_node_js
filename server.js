const express = require("express");
const mongoose = require("mongoose");
const app = express();
const PORT = 4000;
const DB = "mongodb://localhost/blog";
const Schema = mongoose.Schema;
app.listen(PORT, () => console.log(`Example app listening on port ${4000}!`));
mongoose
  .connect(DB, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("successfully connected to DB"));
const postSchema = new Schema({
  title: {
    type: String,
    required: true,
  },
  content: {
    type: String,
    required: true,
  },
  createdAt: {
    type: String,
    index: true,
    unique: true,
  },
  author: String,
  category: String,
});

const Post = mongoose.model("POST", postSchema);
app.get("/", function (req, res) {
  res.send(`<p>Basic Api Rest</p>
  <ul>
<li>All posts - <a href="/api/posts">/api/posts<a> </li>
<li>Single Post : 1 <a href="/api/posts/1">/api/posts/:id<a> </li>
<li>Single Post : 3 <a href="/api/posts/5ee72b400cc2c19ed62a7c26">/api/posts/:id<a> </li>
  </ul>
  `);
});

// méthode de get
app.get("/api", function (req, res) {
  res.send("Welcome to the basic API rest !");
});

app.get("/api/posts", function (req, res) {
  Post.find({}, (error, posts) => {
    if (error) {
      res.status(400).error(error);
      return;
    }

    res.status(200).send({
      response: posts,
    });
  });
});

app.get("/api/posts/:id", function (req, res) {
  const id = req.params.id;
  Post.findById(id, (error, post) => {
    if (error || !post) {
      res.send({
        error: true,
        message: "Post not found",
      });
    } else {
      res.status(200).send({
        response: post,
      });
    }
  });
});

app.post('/api/post/add',function (req, res) {
  const post = new Post({
    title: "new post title",
    content:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed venenatis odio. Suspendisse id sapien sit amet sem tempus ullamcorper vel sollicitudin nisi. Curabitur mauris tortor, rutrum sit amet lobortis ac, lacinia nec eros. Etiam rutrum ligula orci, nec cursus arcu faucibus id. Nullam elit risus, hendrerit eget sapien vel, semper fermentum nulla. Sed efficitur sem malesuada sapien lacinia, porta mollis tellus dictum. Nulla maximus vel mauris quis faucibus. Etiam ac magna varius, tincidunt quam non, feugiat erat. Nulla at placerat quam. Praesent ut imperdiet risus. Vivamus fringilla ipsum at enim venenatis, non faucibus urna sagittis. Vivamus lacinia lectus ut leo pharetra, eu sagittis arcu maximus.",
      createdAt:"Mon Jun 17 2020 09:59:33 GMT+0200",
      author:"by some author",
      category:"design",
  })

  post.save(error =>{
    if(error){
      console.log("Il y a une erreur")
      res.status(400).send({
        error:`error adding new post ${error}`
      })
      return
    }
    res.status(200).send("post succesfully added")
  })
})
